﻿using System;

namespace exercise_21
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task A
            Console.Clear();
            Console.WriteLine("Task A");
            //Declare variable
            var counter1 = 5;

            //for loop
            for (var index1 = 0; index1 < counter1; index1++)
            {
                var a = index1 + 1;
                Console.WriteLine($"This is line number {a}");
            }

            Console.WriteLine();

            //Task B 
            Console.WriteLine("Task B");
            //Declare variable
            var counter2 = 5;
            var index2 = 0;

            //while loop
            while (index2 < counter2)
            {
                var a1 = index2 + 1;
                Console.WriteLine($"This is line number{a1}");

                index2++;
            }

            Console.WriteLine();

            //Task C/1 - for loop displaying even numbers
            Console.WriteLine("Task C/1");
            //Declare variables
            var counter3 = 20;

            //for loop displaying even numbers only up to 20
            for (var index3 = 0; index3 < counter3; index3++)
            {
                var a2 = index3 + 1;

                if (a2%2==0)
                {
                    Console.WriteLine($"This is line number{a2}");
                }
            }

            Console.WriteLine();

            //Task C/2 - while loop displaying even numbers 
            Console.WriteLine("Task C/2");
            //Declare variables
            var counter4 = 20;
            var index4 = 0;

            while (index4 < counter4)
            {
                var a3 = index4 + 1;
            if (a3%2==0)
            {Console.WriteLine($"This is line number {a3}");}
            index4++;    
            }

            Console.WriteLine();
            //Task D - Do while loop with index greater than the counter1
            Console.WriteLine("Task D");
            //Declare variables
            var counter5 = 12;
            var index5 = 18;

            //do while loop
            do
            {
                var a4 = index5
            }

        }
    }
}
